import axiosWrap from  '../utils/axiosWrap'

export default {
   
    getRelationList(data){
        return axiosWrap({
            method:'post',
            url:'/node/show',
            data
        })  
    },
    getCollectList(data){
        return axiosWrap({
            method:'post',
            url:'/node/retract',
            data
        })
    }
  
}