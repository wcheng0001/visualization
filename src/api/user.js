import axiosWrap from  '../utils/axiosWrap'

export default {
    login(data){
        return axiosWrap({
            url:'/user/login',
            method:'post',
            data
        })
    },
    adduser(data){
        return axiosWrap({
            url:'/user',
            method:'post',
            data
        })
    },
    getUserList(data){
        return axiosWrap({
            url:'/user/condition',
            method:'post',
            data
        })
    },
    getUserInfo(){
        return axiosWrap({
            url:'/user/info',
            method:'get',  
        })
    },
    updateUserInfo(data,id){
        return axiosWrap({
            url:`/user/${id}`,
            method:'put',
            data
        })
    },
    getUserLog(data){
        return axiosWrap({
            url:'/systemLog/condition',
            method:'post',
            data
        })
    },
    resetPassword(data){
        return axiosWrap({
            url:'/user/resetPwd',
            method:'put',
            data
        })
    },
    deleteUser(id){
        return axiosWrap({
            url:`/user/${id}`,
            method:'delete',
        }) 
    }
}