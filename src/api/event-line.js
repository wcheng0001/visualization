import axiosWrap from  '../utils/axiosWrap'

export default {
   
    getEventList(data){
        return axiosWrap({
            method:'post',
            url:'/event/eventTimeShow',
            data
        })  
    },
    getEventSelectData(data){
        return axiosWrap({
            method:'post',
            url:'/event/topic',
            data
        })  
    },
    getAllShow(data){
        return axiosWrap({
            method:'post',
            url:'/event/allShow',
            data
        })  
    },
    getMapData(data){
        return axiosWrap({
            method:'post',
            url:'/event/eventTimeMap',
            data
        })    
    },
    getTimeStart(data){
        return axiosWrap({
            method:'get',
            url:'/event/eventTimeShowDate',
            params:{
               ...data
            }
        })     
    },
    getLineMapList(data){
        return axiosWrap({
            method:'post',
            url:'/event/eventTimeLines',
            data
        })   
    }
  
}