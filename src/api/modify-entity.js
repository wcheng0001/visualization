import axiosWrap from  '../utils/axiosWrap'

export default {
    addEntity(data){
        return axiosWrap({
            method:'post',
            url:'/node',
            data
        })
    },
    getSelectData(){
        return axiosWrap({
            method:'get',
            url:'/node/labels'
        })
    },
    getEntityList(data){
        return axiosWrap({
            method:'post',
            url:'/node/condition',
            data
        })
    },
    megreEntity(data){
        return axiosWrap({
            method:'post',
            url:'/node/merge',
            data
        })
    },
    getEntityDetail(id) {
        return axiosWrap({
            method:'get',
            url:`/node/${id}`,
        }) 
    },
    modifyEntityDetail(data) {
        return axiosWrap({
            method:'put',
            url:`/node/${data.id}`,
            data
        }) 
    },
    deleteEntityDetail(id) {
        return axiosWrap({
            method:'delete',
            url:`/node/${id}`,
        }) 
    },
    getSelectList(){
        return axiosWrap({
            method:'get',
            url:'/event/all'
        })
    },
   getSelectListOfid(id){
        return axiosWrap({
            method:'get',
            url:`/node/${id}/labels`
        })
   },
   getSelectListOff(){
        return axiosWrap({
            method:'get',
            url:`/node/labels`
        })
   }

}
 