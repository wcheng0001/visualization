import axiosWrap from  '../utils/axiosWrap'

export default {
    getEventSelectList(data){
        return axiosWrap({
            method:'post',
            url:'/relation/condition',
            data
        })
    },
    getRelationList(){
        return axiosWrap({
            method:'get',
            url:'/relation/types',
        })  
    },
    getRelationDetail(id){
        return axiosWrap({
            method:'get',
            url:`/relation/${id}`,
        })   
    },
    getAllData(data){
       return axiosWrap({
           method:'post',
           url:'/relation/show',
           data
       })
    },
    getRelationData(){
        return axiosWrap({
            method:'get',
            url:`/relation/types`,
        })   
    },
    addRelation(data){
        return axiosWrap({
            method:'post',
            url:`/relation`,
            data
        })   
    },
    deleteRelation(id){
        return axiosWrap({
            method:'delete',
            url:`/relation/${id}`,
        })   
    },
    modifyRelation(id,data){
        return axiosWrap({
            method:'put',
            url:`/relation/${id}`,
            data
        })   
    }
}