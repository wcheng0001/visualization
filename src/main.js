import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/reset.css'
Vue.config.productionTip = false
Vue.use(ElementUI)
router.beforeEach((to, from, next) => {
  //登录过期，未登录
   if(!sessionStorage.getItem('userToken') && to.name != 'login'){
        next({
          path: '/kg/login',
          query: {redirect: to.fullPath}
      })
   }else{
     next()
   }
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
