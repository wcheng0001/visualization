const isLocal = window.location.host.indexOf('localhost') != -1 || window.location.host.indexOf('192') != -1 

let baseUrl = ''
if(isLocal){
 baseUrl =  'http://106.12.179.81:9091'
}else {
 baseUrl = 'http://106.12.179.81:9091'
}
const getToken = function() {
  return  sessionStorage.getItem('userToken') 
}
export default {
    baseURL : baseUrl,
    getToken:getToken
}

