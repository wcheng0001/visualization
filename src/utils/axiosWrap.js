import axios from 'axios';
import appInfo from '../config/config'
import {  Message } from 'element-ui'
import qs from 'qs'

let axiosWrap = axios.create({
    baseURL: appInfo.baseURL
})
axiosWrap.interceptors.request.use((config) => {
        let token = appInfo.getToken()
        if(token){
            config.headers['Authorization'] = token 
        }
        config.headers['contentType'] = 'application/json' 
        // if(config.method === 'post') {
        //     config.data = qs.stringify(config.data);
        //   }
          return config;
      
      },
      error => {
        console.log(error) // for debug
        return Promise.reject(error)
      }
)

axiosWrap.interceptors.response.use((result) => {
     //处理登录过期
     console.log(result.data.code === -403,'   ceshi')
    if (result.data.code === 1602 || result.data.code === -403) {
        sessionStorage.removeItem('userToken') 
        location.reload()
        return Promise.reject(result)
    }
    //登录密码用户名错误
    if (result.data.code ===  -1001 ) {
        Message({
            message:result.data.msg||'用户名或密码错误',
            type: 'error',
            duration: 5 * 1000
          })
        return Promise.reject(result)
    }
    return result
}, result => {
    if(result.response.status === 401 || result.response.status === 403 ){
        sessionStorage.removeItem('userToken') 
        location.reload() 
        return Promise.reject(result) 
    };
    return Promise.reject(result)
})

export default axiosWrap