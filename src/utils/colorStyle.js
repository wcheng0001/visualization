const colorColl = {
    address:'#F79767',
    area:'#57C7E3',
    charge:'#8DCC93',
    event:'#ECB5C9',
    goods:'#4C8EDA',
    organization:'#F16667',
    organiz:'#FFC454',
    other2:'#DA7194',
    person:'#D9C8AE',
    person_type:'#569480',
    sys_info:'#A5ABB6'
}
export default function(type){
    return colorColl[type] ? colorColl[type] :'#D9C8AE'
}