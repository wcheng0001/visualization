import Vue from 'vue'
import Router from 'vue-router'
import Layout from './page/layout.vue'
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

export default new Router({
  //服务端不支持
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect:'/kg'
    },
    {
      path: '/kg',
      name: 'layout',
      component: Layout,
      redirect:'/kg/events',
      children:[
        {
          path: 'entity',
          name: 'layentityout',
          component: () => import('@/page/entity-relationship') , 
        },{
          path: 'modify',
          name: 'modify',
          component: () => import('@/page/modify-entity') , 
        },{
          path: 'system',
          name: 'system',
          component: () => import('@/page/system-setting') , 
        },{
          path: 'events',
          name: 'events',
          component: () => import('@/page/event-devolope') , 
        },{
          path: 'relation',
          name: 'relation',
          component: () => import('@/page/show-entity-relation') , 
        }
      ]
    },
    {
      path: '/kg/login',
      name: 'login',
      component: () => import('@/page/login'), 
    }
  ]
})
