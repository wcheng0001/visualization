import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      formData:{},
      user:null
  },
  mutations: {
    CHANGE_FORM(state,data){
        state.formData = data
    },
    CAHNGE_USER(state,data){
      state.user = data
    }
  },
  actions: {
     change_form({commit},data){
       commit('CHANGE_FORM',data)
     } ,
     change_userInfo({commit},data){
      commit('CAHNGE_USER',data)
     }   
  }
})
