const gulp = require('gulp');


/**
 * 编译代码，自动部署到测试服务器
 */
const ftp = require('gulp-sftp');
const config = require('./ftpConfig');
const del = require('del')

// 正式

gulp.task('uploadToProd', function (callback) {
    console.log('## 正在部署到正式服务器上');
    var dev = config.product;
     return gulp.src('.' + config.publicPath + '**').pipe(ftp(Object.assign(dev, {callback})))
});
